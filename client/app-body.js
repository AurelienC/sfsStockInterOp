import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { name as Magasinier } from '../imports/components/magasinier/magasinier';
import { name as Operateur } from '../imports/components/operateur/operateur';
import { name as Admin } from '../imports/components/admin/admin';


import template from './app-body.html';

const name = 'home';

angular.module(name, [
  uiRouter,
  angularMeteor,
  Magasinier,
  Operateur,
  Admin,
])
.component(name, {
  template,
})
.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', ($stateProvider, $locationProvider, $urlRouterProvider) => {
  'ngInject';

  const stateArray = [];

  // Home
  stateArray.push({
    name: 'home',
    url: '/home',
    template: '<h1>Stock encours</h1>',
  });


  stateArray.push({
    name: 'magasinier',
    url: '/magasinier',
    template: '<magasinier></magasinier>',
  });

  stateArray.push({
    name: 'operateur',
    url: '/operateur',
    template: '<operateur></operateur>',
  });

  stateArray.push({
    name: 'admin',
    url: '/admin',
    template: '<admin></admin>',
  });

  // Inject all states
  _.each(stateArray, (value) => {
    $stateProvider.state(value);
  });


  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
  $urlRouterProvider.otherwise('home');
}]);
