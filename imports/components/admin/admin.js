import angular from 'angular';
import angularMeteor from 'angular-meteor';
import angularMoment from 'angular-moment';
import uiRouter from 'angular-ui-router';
import Chart from 'chart.js';
import { Meteor } from 'meteor/meteor';
import { moment } from 'meteor/momentjs:moment';
import { ReactiveVar } from 'meteor/reactive-var';
import { Containers } from '../../api/containers';
import { Machines } from '../../api/machines';
import { Emplacments } from '../../api/emplacments';
import { Save } from '../../api/save';

import template from './admin.html';

export const name = 'admin';

class AdminCtrl {
  constructor($scope, $document, $window) {
    $scope.viewModel(this);
    $document[0].title = 'Admin'; // eslint-disable-line

    this.subscribe('containers');
    this.subscribe('machines');
    this.subscribe('emplacments');
    this.subscribe('save');

    this.window = $window;
    this.skip = 0;
    this.limit = 10;
    this.state = 'all';
    this.limitReac = new ReactiveVar(this.limit);
    this.skipReac = new ReactiveVar(this.skip);
    this.poReac = new ReactiveVar(null);
    this.articleReac = new ReactiveVar(null);
    this.locationReac = new ReactiveVar(null);
    this.machineReac = new ReactiveVar(null);
    this.lotReac = new ReactiveVar(null);
    this.stateReac = new ReactiveVar(this.state);
    this.check = null;
    this.edit = {};


    // Notifcations
    toastr.options = {
      timeOut: 1000,
      newestOnTop: true,
    };

    this.helpers({
      containers() {
        const po = Number(this.poReac.get());
        const article = Number(this.articleReac.get());
        const location = this.locationReac.get();
        const machine = Number(this.machineReac.get());
        const lot = Number(this.lotReac.get());
        const state = this.stateReac.get();

        Meteor.call('containers.mvt', moment().subtract(7, 'days').toDate(), moment().toDate(), (err, res) => {
          this.startI = moment().subtract(7, 'days').format('DD/MM/YYYY');
          this.endI = moment().format('DD/MM/YYYY');
          this.start = this.startI;
          this.end = this.endI;
          if (!err) this.mvt = res;
          this.updateChart();
        });

        const parametersQuery = {};

        if (!_.isUndefined(po) && po > 0) {
          _.extend(parametersQuery, { po });
        }

        if (!_.isUndefined(article) && article > 0) {
          _.extend(parametersQuery, { article });
        }

        if (!_.isUndefined(location) && !_.isNull(location) && !_.isEmpty(String(location))) {
          _.extend(parametersQuery, { location: String(location) });
        }

        if (!_.isUndefined(machine) && machine >= 40000) {
          _.extend(parametersQuery, { machine });
        }

        if (!_.isUndefined(lot) && lot > 0) {
          _.extend(parametersQuery, { lot });
        }

        switch (state) {
          case 'all':
          break;
          case 'in':
          _.extend(parametersQuery, {
            inDate: { $ne: null },
            requestDate: { $eq: null },
            outDate: { $eq: null },
          });
          break;
          case 'request':
          _.extend(parametersQuery, {
            inDate: { $ne: null },
            requestDate: { $ne: null },
            outDate: { $eq: null },
          });
          break;
          case 'out':
          _.extend(parametersQuery, {
            inDate: { $ne: null },
            requestDate: { $ne: null },
            outDate: { $ne: null },
          });
          break;
          default:
        }

        return Containers.find(parametersQuery, {
          limit: this.limitReac.get() || 10,
          skip: this.skipReac.get() || 0,
        });
      },
      allContainers() {
        const all = Containers.find({}, { limit: 100 });
        const arr = [];

        all.forEach((e) => {
          if (!_.isUndefined(e.outDate)) arr.push(_.omit(e, ['inDate', 'requestDate']));
          if (!_.isUndefined(e.requestDate)) arr.push(_.omit(e, ['inDate', 'outDate']));
          if (!_.isUndefined(e.inDate)) arr.push(_.omit(e, ['requestDate', 'outDate']));
        });

        return _.sortBy(arr, (e) => {
          if (!_.isUndefined(e.outDate)) return moment().diff(e.outDate);
          if (!_.isUndefined(e.requestDate)) return moment().diff(e.requestDate);
          if (!_.isUndefined(e.inDate)) return moment().diff(e.inDate);
          return null;
        });
      },
      machines() {
        return Machines.find();
      },
      emplacments() {
        return Emplacments.find();
      },
      save() {
        return Save.findOne();
      },
      mvt() {
        return this.res || null;
      },
      start() {
        return this.start || null;
      },
      end() {
        return this.end || null;
      },
    });
  }


  updateChart() {
    const dsIn = [];
    const dsOut = [];
    const dsLine = [];
    const dsDates = [];
    this.mvt.forEach((e) => {
      (!_.isUndefined(e.in)) ? dsIn.push(e.in) : dsIn.push(0); // eslint-disable-line
      (!_.isUndefined(e.out)) ? dsOut.push(e.out) : dsOut.push(0); // eslint-disable-line
      dsLine.push((e.in || 0) + (e.out || 0));
      dsDates.push(moment(e.date).format('DD/MM/YY'));
    });


    // chart
    if (!_.isUndefined(this.chart)) this.chart.destroy();
    const chart = angular.element('#chart');
    this.chart = new Chart(chart, {
      type: 'bar',
      data: {
        labels: dsDates,
        datasets: [
          {
            type: 'line',
            label: 'Total',
            data: dsLine,
            borderColor: 'rgba(86, 61, 124, 0.5)',
            fill: false,
          }, {
            type: 'bar',
            label: 'Entrées',
            backgroundColor: 'rgba(54, 162, 235, 0.5)',
            data: dsIn,
          }, {
            type: 'bar',
            label: 'Sorties',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
            data: dsOut,
          },
        ],
      },
      options: {
        title: {
          display: true,
          text: 'Mouvements d\'entrées/sorties',
        },
        responsive: true,
        legend: {
          display: true,
        },
        scales: {
          xAxes: [{
            stacked: true,
          }],
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true,
            },
          }],
        },
      },
    });
  }

  newDate() {
    this.mvt = null;
    this.start = moment(this.startI, 'DD/MM/YYYY')
    .hour(0).minute(0).second(0)
    .millisecond(0);
    this.end = moment(this.endI, 'DD/MM/YYYY')
    .hour(23).minute(59).second(59)
    .millisecond(999);
    Meteor.call('containers.mvt', this.start.toDate(), this.end.toDate(), (err, res) => {
      if (!err) this.mvt = res;
    });
    this.updateChart();
  }

  exportChart() {
    this.window.open(angular.element('#chart')[0].toDataURL());
  }

  change() {
    this.limitReac.set(Number(this.limit));
    this.skipReac.set(Number(this.skip));
    this.poReac.set(Number(this.po));
    this.articleReac.set(Number(this.article));
    this.locationReac.set(this.location);
    this.machineReac.set(Number(this.machine));
    this.lotReac.set(Number(this.lot));
    this.stateReac.set(this.state);
  }

  open(edit) {
    this.edit = edit;
    angular.element('#myModal').modal('show');
  }

  close() {
    angular.element('#myModal').modal('hide');
  }

  in(id) {
    Meteor.call('containers.in', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Entrée réussie.', 'Succès', { timeOut: 5000 });
    });
  }

  request(id) {
    Meteor.call('containers.request', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Demande réussie.', 'Succès', { timeOut: 5000 });
    });
  }

  out(id) {
    Meteor.call('containers.out', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Sortie réussie.', 'Succès', { timeOut: 5000 });
    });
  }

  remove(id) {
    Meteor.call('containers.remove', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) this.close();
    });
  }

  cancelRequest(id) {
    Meteor.call('containers.cancelRequest', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Annulation réussie.', 'Succès', { timeOut: 5000 });
    });
  }

  cancelOut(id) {
    Meteor.call('containers.cancelOut', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Annulation réussie.', 'Succès', { timeOut: 5000 });
    });
  }

  addMachine(m) {
    Meteor.call('machines.insert', Number(m.id), String(m.emplacment).toUpperCase(), (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Ajout réussi.', 'Succès', { timeOut: 5000 });
    });
    this.new = {};
  }

  removeMachine(id) {
    Meteor.call('machines.remove', Number(id), (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Suppression effectuée.', 'Succès', { timeOut: 5000 });
    });
  }

  machineName(arg) {
    if (_.isUndefined(arg)) return null;

    const value = Machines.findOne({ _id: arg.toString() });
    if (_.isUndefined(value)) {
      return null;
    }
    return value.emplacment;
  }

  updateSave(s) {
    Meteor.call('save.update', s._id, s, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Mise à jour effectuée.', 'Succès', { timeOut: 5000 });
    });
  }

  addEmplacments(value) {
    const patt = /^[0-9]{2}[A-Z][0-9]{2}$/;
    const values = value.split(/\r\n|\r|\n/g);

    values.forEach((elem) => {
      if (!_.isEmpty(elem)) {
        if (patt.test(elem)) {
          Meteor.call('emplacments.insert', elem, (err) => {
            if (err) toastr.error(`Erreur ajout ${elem} : ${err.message}`, 'Erreur', { timeOut: 5000 });
          });
        } else {
          toastr.error(`Erreur ajout ${elem} !`, 'Erreur', { timeOut: 5000 });
        }
      }
    });
  }

  removeEmplacment(id) {
    Meteor.call('emplacments.remove', String(id), (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Suppression emplacement réussi.', 'Succès', { timeOut: 5000 });
    });
  }

  export() {
    Meteor.call('containers.export', (err, fileContent) => {
      if (fileContent) {
        const blob = new Blob([fileContent], { type: 'text/csv;charset=utf-8' }); // eslint-disable-line
        saveAs(blob, 'export.csv');  // eslint-disable-line
      }
    });
  }

  exportInventaire() {
    Meteor.call('containers.exportInventaire', (err, fileContent) => {
      if (fileContent) {
        const blob = new Blob([fileContent], { type: 'text/csv;charset=utf-8' }); // eslint-disable-line
        saveAs(blob, 'exportInventaire.csv');  // eslint-disable-line
      }
    });
  }

  import() {
    const file = angular.element('#fileCsv')[0].files[0];
    if (_.isUndefined(file)) {
      toastr.error('Champ vide', 'Erreur', { timeOut: 3000 });
    } else {
      const reader = new FileReader(); // eslint-disable-line
      reader.readAsText(file);
      reader.onload = (e) => {
        Meteor.call('containers.import', e.target.result, (err) => {
          if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
          if (!err) toastr.success('Import réussi.', 'Succès', { timeOut: 5000 });
        });
      };
    }
  }
}

export default angular.module(name, [
  angularMeteor,
  angularMoment,
  uiRouter,
])
.component(name, {
  templateUrl: template,
  controller: ['$scope', '$document', '$window', AdminCtrl],
})
/* eslint-disable */
.filter('po', () => {
  return (arg) => {
    if (_.isUndefined(arg)) return null;
    return arg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
})
.filter('chooseDate', () => {
  return (arg) => {
    if (_.isUndefined(arg)) return null;
    if (!_.isUndefined(arg.outDate)) return arg.outDate;
    if (!_.isUndefined(arg.requestDate)) return arg.requestDate;
    if (!_.isUndefined(arg.inDate)) return arg.inDate;
  }
});
