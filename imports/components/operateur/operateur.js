import angular from 'angular';
import angularMeteor from 'angular-meteor';
import angularMoment from 'angular-moment';
import uiRouter from 'angular-ui-router';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Containers } from '../../api/containers';
import { Machines } from '../../api/machines';

import template from './operateur.html';

export const name = 'operateur';

class OperateurCtrl {
  constructor($scope, $document) {
    $scope.viewModel(this);
    $document[0].title = 'Opérateur'; // eslint-disable-line

    this.subscribe('containersRequested');
    this.subscribe('machines');

    this.document = $document;
    this.choice = 0;
    this.encours = {};
    this.composant = {};
    this.machine = new ReactiveVar(0);
    this.machineTemp = this.machine.get();

    // Notifcations
    toastr.options = {
      timeOut: 1000,
      newestOnTop: true,
    };

    // Focus to barcode input
    angular.element('#barcode-input').blur((e) => {
      e.preventDefault();
      setTimeout(() => { // eslint-disable-line
        if (!angular.element('.no-focus').is(':focus')) {
          angular.element('#barcode-input').focus();
        }
      }, 1);
    });

    angular.element('.no-focus').blur((e) => {
      e.preventDefault();
      setTimeout(() => { // eslint-disable-line
        if (!angular.element('.no-focus').is(':focus')) {
          angular.element('#barcode-input').focus();
        }
      }, 1);
    });


    // Entrée code-barres
    angular.element('#barcode-form').submit((event) => {
      event.preventDefault();

      const value = Number(this.barcode);
      const patt = /^[0-9]+.0$/;

      if (this.barcode === 'OK') {
        if (this.choice === 1) this.addEncours();
        if (this.choice === 2) this.addComposant();
      } else if (this.barcode === 'RAZ' && (this.choice === 1 || this.choice === 2)) {
        this.clearForms();
        toastr.success('Nettoyage valeurs.', 'Entrée code-barres');
      } else if (this.barcode === 'ENCOURS' && this.machine.get()) {
        this.choice = 1;
        this.clearForms();
      } else if (this.barcode === 'COMPOSANT' && this.machine.get()) {
        this.choice = 2;
        this.clearForms();
      } else if (this.barcode === 'SORTIE' && this.machine.get()) {
        this.choice = 3;
        this.clearForms();
      } else if (this.barcode.length === 5 && value > 39999 && value < 41000) { // Machine
        this.encours.machine = value;
        this.choice = 0;
        this.machineTemp = value;
        this.changeMachine();
        toastr.success(`Machine ${value}`, 'Entrée code-barres');
      } else if (patt.test(this.barcode) && this.choice === 2) {  // Quantité
        this.composant.parts = value;
        toastr.success(`Pièces ${value}`, 'Entrée code-barres');
      } else if (this.barcode.length >= 4 && this.barcode.length <= 7 && this.choice === 2) { // eslint-disable-line
        this.composant.article = value;  // Composant
        this.findParts();
        toastr.success(`Composant ${value}`, 'Entrée code-barres');
      } else if (this.barcode.length === 8 && this.choice === 1) { // OF
        this.encours.po = value;
        this.findPo();
        toastr.success(`OF ${value}`, 'Entrée code-barres');
      } else { // Non reconnu
        toastr.error('Valeur non valide ou non reconnue', 'Entrée code-barres');
      }
      this.barcode = '';
      angular.element('#barcode-input').focus();
    });

    this.helpers({
      choice() {
        return this.choice;
      },
      containers() {
        const values = Containers.find({
          $and: [
            { machine: this.machine.get() },
            { requestDate: { $ne: null } },
            { outDate: { $eq: null } },
          ],
        }, {
          sort: { requestDate: 1 },
        });

        values.observe({
          removed: (doc) => {
            toastr.warning(`${doc.po ? 'OF' : 'Composant'} ${doc.po || doc.article} sorti du stock.`, 'Sortie de stock', { timeOut: 3000 });
          },
        });

        this.document[0].title = `Opérateur - ${this.machine.get()} (${values.count()})`;

        return values;
      },
      machineName() {
        const value = Machines.findOne({ _id: this.machine.get().toString() });
        if (_.isUndefined(value)) return null;
        return value.emplacment;
      },
      sumOfParts() {
        return this.sumOfParts || 0;
      },
      sumOfPo() {
        return this.sumOfPo || 0;
      },
    });
  }

  clearForms() {
    this.encours = {};
    this.composant = {};
    this.sumOfPo = null;
    this.sumOfParts = null;
    angular.element('input[name=po], input[name=article], input[name=parts]').val(null);
  }

  changeMachine() {
    this.machine.set(Number(this.machineTemp));
    this.clearForms();
    angular.element('#barcode-input').focus();
  }

  changeChoice(value) {
    this.choice = Number(value);
    this.clearForms();
    angular.element('#barcode-input').focus();
  }

  addEncours() {
    Meteor.call('containers.opRequestEncours', Number(this.machine.get()), Number(this.encours.po), (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) {
        toastr.success('Demande de sortie de stock effectuée.', 'Succès', { timeOut: 5000 });
        this.clearForms();
      }
    });

    angular.element('#barcode-input').focus();
  }

  addComposant() {
    Meteor.call('containers.opRequestComposant', Number(this.machine.get()), Number(this.composant.article), Number(this.composant.parts), (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) {
        toastr.success('Demande de sortie de stock effectuée.', 'Succès', { timeOut: 5000 });
        this.clearForms();
      }
    });
    angular.element('#barcode-input').focus();
  }

  findParts() {
    if (!_.isUndefined(this.composant.article)) {
      Meteor.call('containers.sumOfParts', Number(this.composant.article), (err, res) => {
        if (!err) this.sumOfParts = res;
      });
    }
  }

  findPo() {
    if (!_.isUndefined(this.encours.po)) {
      Meteor.call('containers.findPo', Number(this.machine.get()), Number(this.encours.po), (err, res) => {
        if (!err) this.sumOfPo = res;
      });
    }
  }
}


export default angular.module(name, [
  angularMeteor,
  angularMoment,
  uiRouter,
])
.component(name, {
  templateUrl: template,
  controller: ['$scope', '$document', OperateurCtrl],
})
/* eslint-disable */
.filter('po', () => {
  return (arg) => {
    if (_.isUndefined(arg)) return null;
    return arg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
  /* eslint-enable */
});
