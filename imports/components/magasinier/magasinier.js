import angular from 'angular';
import angularMeteor from 'angular-meteor';
import angularMoment from 'angular-moment';
import uiRouter from 'angular-ui-router';
import { Meteor } from 'meteor/meteor';
import { Containers } from '../../api/containers';
import { Machines } from '../../api/machines';
import { Emplacments } from '../../api/emplacments';

import template from './magasinier.html';

export const name = 'magasinier';

class MagasinierCtrl {
  constructor($scope, $document) {
    $scope.viewModel(this);
    $document[0].title = 'Magasinier'; // eslint-disable-line

    this.subscribe('containersRequested');
    this.subscribe('containersLastOut');
    this.subscribe('containersNotFreeEmplacments');

    this.subscribe('emplacments');
    this.subscribe('machines');

    this.document = $document;
    this.choice = 0;
    this.encours = {};
    this.composant = {};

    // Notifcations
    toastr.options = {
      timeOut: 1000,
      newestOnTop: true,
    };

    // Focus to barcode input
    angular.element('#barcode-input').blur((e) => {
      e.preventDefault();
      setTimeout(() => { // eslint-disable-line
        if (!angular.element('.no-focus').is(':focus')) {
          angular.element('#barcode-input').focus();
        }
      }, 1);
    });

    angular.element('.no-focus').blur((e) => {
      e.preventDefault();
      setTimeout(() => { // eslint-disable-line
        if (!angular.element('.no-focus').is(':focus')) {
          angular.element('#barcode-input').focus();
        }
      }, 1);
    });


    toastr.options = {
      timeOut: 1000,
      newestOnTop: true,
    };

    // Entrée code-barres
    angular.element('#barcode-form').submit((event) => {
      event.preventDefault();

      const value = Number(this.barcode);
      const patt = /^[0-9]+.0$/;

      if (this.barcode === 'OK') {
        if (this.choice === 1) this.addEncours();
        if (this.choice === 2) this.addComposant();
      } else if (this.barcode === 'ENCOURS') {
        this.choice = 1;
        this.clearForms();
      } else if (this.barcode === 'COMPOSANT') {
        this.choice = 2;
        this.clearForms04();
      } else if (this.barcode === 'SORTIE') {
        this.choice = 3;
      } else if (this.barcode === 'RAZ' && (this.choice === 1 || this.choice === 2)) {
        this.clearForms();
        toastr.success('Nettoyage valeurs.', 'Entrée code-barres');
      } else if (patt.test(this.barcode) && this.choice === 2) { // Quantité
        this.composant.parts = value;
        toastr.success(`Pièces ${value}`, 'Entrée code-barres');
      } else if (this.barcode.length === 10 && this.choice === 2) { // Lot
        this.composant.lot = value;
        toastr.success(`Lot ${value}`, 'Entrée code-barres');
      } else if (this.barcode.length === 5 && value > 39999 && value < 41000 && this.choice === 1) { // eslint-disable-line
        this.encours.machine = value; // Machine
        toastr.success(`Machine ${value}`, 'Entrée code-barres');
      } else if (this.barcode.length === 7 && this.choice === 2) { // Composant
        this.composant.article = value;
        toastr.success(`Composant ${value}`, 'Entrée code-barres');
      } else if (this.barcode.length === 8 && this.choice === 1) { // OF
        this.encours.po = value;
        toastr.success(`OF ${value}`, 'Entrée code-barres');
      } else { // Non reconnu
        toastr.error('Valeur non valide ou non reconnue', 'Entrée code-barres');
      }
      this.barcode = '';
      angular.element('#barcode-input').focus();
    });


    // Notification
    Containers.find({
      $and: [
        { requestDate: { $ne: null } },
        { outDate: { $eq: null } },
      ],
    }).observe({
      added: (doc) => {
        toastr.warning(`Demande sortie ${doc.po ? 'OF' : 'composant'} ${doc.po || doc.article} [${doc.location}] par ${doc.machine}.`, 'Sortie de stock', { timeOut: 3000 });
      },
    });


    this.helpers({
      containers() {
        const values = Containers.find({
          $and: [
            { requestDate: { $ne: null } },
            { outDate: { $eq: null } },
          ],
        }, { sort: { inDate: 1 } });

        this.document[0].title = `Magasinier (${values.count()})`;

        return values;
      },
      containersLastOut() {
        return Containers.find({
          $and: [
            { requestDate: { $ne: null } },
            { outDate: { $ne: null } },
          ],
        }, { sort: { outDate: -1 }, limit: 5 });
      },
      containersLastIn() {
        return Containers.find({
          $and: [
            { inDate: { $ne: null } },
            { requestDate: { $eq: null } },
            { outDate: { $eq: null } },
          ],
        }, { sort: { inDate: -1 }, limit: 5 });
      },
      free() {
        const notFree = [];
        const empl = [];

        const notFreeResult = Containers.find({
          $and: [
            { inDate: { $ne: null } },
            { outDate: { $eq: null } },
          ],
        }, {
          fields: { location: 1 },
        });

        const emplResult = Emplacments.find();

        this.rate = notFreeResult.count() / emplResult.count();

        notFreeResult.forEach((e) => {
          notFree.push(e.location);
        });

        emplResult.forEach((e) => {
          empl.push(e._id);
        });

        return _.shuffle(_.difference(empl, notFree).slice(0, 8));
      },
      rate() {
        return this.rate || null;
      },
      choice() {
        return this.choice;
      },
      testOfEmplacment() {
        return this.testOfEmplacment || false;
      },
    });
  }

  clearForms() {
    this.encours = {};
    this.composant = {};
    angular.element('input[name=po], input[name=machine], input[name=location], input[name=article], input[name=lot], input[name=parts]').val(null);
  }

  changeChoice(value) {
    this.choice = Number(value);
    angular.element('#barcode-input').focus();
    this.clearForms();
  }

  addEncours() {
    Meteor.call('containers.insert', _.extend(this.encours, { inDate: new Date() }), (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Ajout encours réussi.', 'Succès', { timeOut: 5000 });
    });
    this.clearForms();
    angular.element('#barcode-input').focus();
  }

  addComposant() {
    Meteor.call('containers.insert', _.extend(this.composant, { inDate: new Date() }), (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Ajout composant réussi.', 'Succès', { timeOut: 5000 });
    });
    this.clearForms();
    angular.element('#barcode-input').focus();
  }

  out(id) {
    Meteor.call('containers.out', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Sortie réussie.', 'Succès', { timeOut: 5000 });
    });
    angular.element('#barcode-input').focus();
  }

  cancelOut(id) {
    Meteor.call('containers.cancelOut', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Annulation sortie réussie.', 'Succès', { timeOut: 5000 });
    });
    angular.element('#barcode-input').focus();
  }

  cancelRequest(id) {
    Meteor.call('containers.cancelRequest', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Annulation demande réussie.', 'Succès', { timeOut: 5000 });
    });
    angular.element('#barcode-input').focus();
  }

  modalRemove(e) {
    this.edit = e;
    angular.element('#myModal').modal('show');
  }

  remove(id) {
    Meteor.call('containers.remove', id, (err) => {
      if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
      if (!err) toastr.success('Annulation entrée réussie.', 'Succès', { timeOut: 5000 });
    });
    this.edit = {};
    angular.element('#myModal').modal('hide');
    angular.element('#barcode-input').focus();
  }

  machineName(arg) {
    if (_.isUndefined(arg)) return null;

    const value = Machines.findOne({ _id: arg.toString() });
    if (_.isUndefined(value)) {
      return null;
    }
    return value.emplacment;
  }

  testEmplacment(value) {
    if (!_.isUndefined(value)) {
      Meteor.call('containers.testEmplacment', value, (err, res) => {
        if (err) toastr.error(err.message, 'Erreur', { timeOut: 5000 });
        this.testOfEmplacment = res;
      });
    }
  }
}

export default angular.module('magasinier', [
  angularMeteor,
  angularMoment,
  uiRouter,
])
.component(name, {
  templateUrl: template,
  controller: ['$scope', '$document', MagasinierCtrl],
})
/* eslint-disable */
.filter('po', () => {
  return (arg) => {
    if (_.isUndefined(arg)) return null;
    return arg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
})
.filter('percentage', () => {
  return (input, decimals) => {
    if (_.isUndefined(Number(input)) || _.isUndefined(Number(decimals))) return null;
    return `${(Number(input) * 100).toFixed(Number(decimals))}%`;
  }
});
  /* eslint-enable */
