import SMB2 from 'smb2';
import fs from 'fs';
import path from 'path';
import Client from 'ftp';

import { Meteor } from 'meteor/meteor';
import { moment } from 'meteor/momentjs:moment';
import { exportcsv } from 'meteor/lfergon:exportcsv';
import { Containers } from './containers';
import { Save } from './save';

if (Meteor.isServer) {
  // FTP Save


  function ftpSave(ftp, buffer, file) { // eslint-disable-line
    if (ftp.active) {
      const c = new Client();
      c.on('ready', () => {
        c.put(buffer, `./${file}`, (err) => {
          if (err) console.error(err);
          c.end();
        });
      });
      c.connect({
        host: ftp.host,
        port: ftp.port || 21,
        user: ftp.user,
        password: ftp.password,
      });
    }
  }

  function localSave(local, buffer, file) { // eslint-disable-line
    if (local.active) {
      const filePath = path.join(local.directory, file);
      fs.access(local.directory, fs.F_OK | fs.W_OK, (err) => { // eslint-disable-line
        if (err) {
          console.error('You must create and give access to server to folder');
        } else {
          fs.writeFileSync(filePath, buffer);
        }
      });
    }
  }

  function smbSave(smb, buffer, file) { // eslint-disable-line
    if (smb.active) {
      const smb2Client = new SMB2({
        share: smb.share,
        domain: smb.domain,
        username: smb.username,
        password: smb.password,
      });

      smb2Client.writeFile(`${smb.directory}\\${file}`, buffer, { encoding: 'utf-8' }, (err) => {
        if (err) console.error(err);
      });
      smb2Client.close();
    }
  }

  exports.save = () => {
    const dataInStock = Containers.find({
      $and: [
        { inDate: { $ne: null } },
        { outDate: { $eq: null } },
      ],
    });

    const dataLastOut = Containers.find({
      $and: [
        { outDate: { $ne: null } },
      ],
    }, {
      sort: { outDate: -1 },
      limit: 50,
    });

    const result = [];

    result.push([
      'id',
      'emplacement',
      'of',
      'article',
      'machine',
      'lot',
      'pieces',
      'entree',
      'demande',
      'sortie',
    ]);

    dataInStock.forEach((e) => {
      result.push([
        e._id || '',
        e.location || '',
        e.po || '',
        e.article || '',
        e.machine || '',
        e.lot || '',
        e.parts || '',
        (e.inDate) ? moment(e.inDate).format('DD/MM/YYYY HH:mm:ss') : '',
        (e.requestDate) ? moment(e.requestDate).format('DD/MM/YYYY HH:mm:ss') : '',
        (e.outDate) ? moment(e.outDate).format('DD/MM/YYYY HH:mm:ss') : '',
      ]);
    });

    dataLastOut.forEach((e) => {
      result.push([
        e._id || '',
        e.location || '',
        e.po || '',
        e.article || '',
        e.machine || '',
        e.lot || '',
        e.parts || '',
        (e.inDate) ? moment(e.inDate).format('DD/MM/YYYY HH:mm:ss') : '',
        (e.requestDate) ? moment(e.requestDate).format('DD/MM/YYYY HH:mm:ss') : '',
        (e.outDate) ? moment(e.outDate).format('DD/MM/YYYY HH:mm:ss') : '',
      ]);
    });


    try {
      const csv = exportcsv.exportToCSV(result, true, ';');
      const buffer = new Buffer(csv);
      const fileName = `${moment().format('YYYY_MM_DD_hhmmss')}.csv`;
      const save = Save.findOne();
      if (save.ftp.active) ftpSave(save.ftp, buffer, fileName);
      if (save.local.active) localSave(save.local, buffer, fileName);
      if (save.smb.active) smbSave(save.smb, buffer, fileName);
    } catch (e) {
      console.error(e);
    }
  };
}
