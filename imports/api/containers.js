import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
import { check, Match } from 'meteor/check';
import { exportcsv } from 'meteor/lfergon:exportcsv';
import parse from 'csv-parse';

import { Emplacments } from './emplacments';


export const Containers = new Mongo.Collection('containers');
const Mvt = new Mongo.Collection(null);

if (Meteor.isServer) {
  import Export from './export';

  // Pour magasinier et admin -> tout
  Meteor.publish('containers', () => { // eslint-disable-line
    return Containers.find();
  });

  // Pour opérateur -> tout ce qui est demandé
  Meteor.publish('containersRequested', () => { // eslint-disable-line
    return Containers.find({
      $and: [
        { outDate: { $eq: null } },
      ],
    });
  });

  // Pour magasinier -> tout ce qui est sorti depuis peu
  Meteor.publish('containersLastOut', () => { // eslint-disable-line
    return Containers.find({
      $and: [
        { requestDate: { $ne: null } },
        { outDate: { $ne: null } },
      ],
    }, {
      sort: { outDate: -1 },
      limit: 10,
    });
  });

  // Pour magasinier -> emplacements occupés
  Meteor.publish('containersNotFreeEmplacments', () => { // eslint-disable-line
    return Containers.find({
      $and: [
        { inDate: { $ne: null } },
        { outDate: { $eq: null } },
      ],
    }, {
    });
  });


  const pattern = {
    location: Match.Where(x => /^[0-9]{2}[A-Z][0-9]{2}$/.test(x)),
    po: Match.Optional(Match.Where((x) => {
      check(x, Number);
      return x.toString().length === 8;
    })),
    article: Match.Optional(Match.Where((x) => {
      check(x, Number);
      return x.toString().length >= 4 && x.toString().length <= 7 && (x >= 50000 || x < 40000);
    })),
    machine: Match.Optional(Match.Where((x) => {
      check(x, Number);
      return x.toString().length === 5 && x >= 40000 && x < 50000;
    })),
    lot: Match.Optional(Number),
    parts: Match.Optional(Number),
    inDate: Date,
    requestDate: Match.Optional(Date),
    outDate: Match.Optional(Date),
  };

  Meteor.methods({
    'containers.insert'(container) { // eslint-disable-line
      check(container, pattern);

      if (_.isUndefined(Emplacments.findOne({ _id: container.location }))) {
        throw new Meteor.Error('emplacement-inexistant', 'L\'emplacement n\'existe pas.');
      }

      const value = Containers.findOne({
        $and: [
          { location: container.location },
          { inDate: { $ne: null } },
          { outDate: { $eq: null } },
        ],
      });


      if (!_.isUndefined(value)) {
        throw new Meteor.Error('emplacement-non-vide', 'L\'emplacement n\'est pas libre.');
      }

      Containers.insert(container, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.remove'(id) { // eslint-disable-line
      check(id, String);
      Containers.remove({ _id: id }, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.in'(id) { // eslint-disable-line
      check(id, String);

      Containers.update(id, { $set: {
        inDate: new Date(),
        requestDate: null,
        outDate: null,
      } }, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.out'(id) { // eslint-disable-line
      check(id, String);
      Containers.update(id, { $set: { outDate: new Date() } }, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.cancelOut'(id) { // eslint-disable-line
      check(id, String);
      Containers.update(id, { $set: {
        outDate: null,
      } }, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.request'(id) { // eslint-disable-line
      check(id, String);
      Containers.update(id, { $set: {
        requestDate: new Date(),
        outDate: null,
      } }, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.cancelRequest'(id) { // eslint-disable-line
      check(id, String);
      Containers.update(id, { $set: {
        requestDate: null,
        outDate: null,
      } }, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.opRequestEncours'(machine, po) { // eslint-disable-line
      check(machine, Number);
      check(po, Number);
      const finded = Containers.findOne({
        $and: [
          { machine },
          { po },
          { requestDate: { $eq: null } },
          { outDate: { $eq: null } },
        ],
      }, {
        sort: { inDate: -1 },
      });

      if (_.isUndefined(finded)) {
        throw new Meteor.Error('pas-de-container-trouvé', 'Aucun container trouvé');
      }

      Containers.update(finded._id, { $set: { requestDate: new Date() } }, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.opRequestComposant'(machine, article, parts) { // eslint-disable-line
      check(machine, Number);
      check(article, Number);
      check(parts, Number);

      const finded = Containers.find({
        $and: [
          { article },
          { requestDate: { $eq: null } },
          { outDate: { $eq: null } },
        ],
      }, {
        sort: { lot: 1, inDate: 1 },
      });

      if (finded.count() <= 0) {
        throw new Meteor.Error('pas-de-container-trouvé', 'Aucun container trouvé');
      }

      let total = 0;
      const out = [];
      finded.forEach((elem) => {
        if (total < parts) {
          out.push(elem._id);
          total += elem.parts;
        }
      });

      if (total < parts) {
        throw new Meteor.Error('pas-assez-de-pieces', `Il manque des pieces (${total} en stock)`);
      }

      _.each(out, (elem) => {
        Containers.update(elem, { $set: {
          requestDate: new Date(),
          machine,
        } });
      }, (err) => {
        if (!err) Export.save();
      });
    },
    'containers.sumOfParts'(art) { // eslint-disable-line
      check(art, Number);
      if (_.isNull(art)) {
        return 0;
      }

      const pipeline = [
        { $match: { $and:
          [
            { article: { $eq: art } },
            { requestDate: { $eq: null } },
            { outDate: { $eq: null } },
          ] } },
          { $group: { _id: '$article', total: { $sum: '$parts' } } },
        ];

        const value = Containers.aggregate(pipeline);
        return _.isEmpty(value) ? 0 : value[0].total || 0;
      },
      'containers.testEmplacment'(emplacment) { // eslint-disable-line
        check(emplacment, String);

        if (_.isUndefined(Emplacments.findOne({ _id: emplacment }))) {
          return false; // N'existe pas
        }

        const value = Containers.findOne({
          $and: [
            { location: emplacment },
            { inDate: { $ne: null } },
            { outDate: { $eq: null } },
          ],
        });

        if (!_.isUndefined(value)) {
          return false;
        }

        return true; // Free emplacment
      },
      'containers.findPo'(machine, po) { // eslint-disable-line
        if (!_.isUndefined(machine) && !_.isUndefined(po)) {
          return Containers.find({
            $and: [
              { machine },
              { po },
              { requestDate: { $eq: null } },
              { outDate: { $eq: null } },
            ],
          }, {
            fields: { _id: 1 },
          }).count() || 0;
        }
        return 0;
      },
      'containers.export'() { // eslint-disable-line
        const values = Containers.find();
        const result = [];

        result.push([
          'id',
          'emplacement',
          'of',
          'article',
          'machine',
          'lot',
          'pieces',
          'entree',
          'demande',
          'sortie',
        ]);

        values.forEach((e) => {
          result.push([
            e._id || '',
            e.location || '',
            e.po || '',
            e.article || '',
            e.machine || '',
            e.lot || '',
            e.parts || '',
            (e.inDate) ? moment(e.inDate).format('DD/MM/YYYY HH:mm:ss') : '',
            (e.requestDate) ? moment(e.requestDate).format('DD/MM/YYYY HH:mm:ss') : '',
            (e.outDate) ? moment(e.outDate).format('DD/MM/YYYY HH:mm:ss') : '',
          ]);
        });

        return exportcsv.exportToCSV(result, true, ';');
      },
      'containers.exportInventaire'() { // eslint-disable-line
        const values = Containers.find({
          $and: [
            { inDate: { $ne: null } },
            { outDate: { $eq: null } },
          ],
        }, {
          sort: { location: 1 },
        });
        const result = [];

        result.push([
          'id',
          'emplacement',
          'of',
          'article',
          'machine',
          'lot',
          'pieces',
          'entree',
          'demande',
          'sortie',
        ]);

        values.forEach((e) => {
          result.push([
            e._id || '',
            e.location || '',
            e.po || '',
            e.article || '',
            e.machine || '',
            e.lot || '',
            e.parts || '',
            (e.inDate) ? moment(e.inDate).format('DD/MM/YYYY HH:mm:ss') : '',
            (e.requestDate) ? moment(e.requestDate).format('DD/MM/YYYY HH:mm:ss') : '',
            (e.outDate) ? moment(e.outDate).format('DD/MM/YYYY HH:mm:ss') : '',
          ]);
        });

        return exportcsv.exportToCSV(result, true, ';');
      },
      'containers.import'(elem) { // eslint-disable-line
        Export.save();

        parse(elem, { delimiter: ';' }, Meteor.bindEnvironment((err, data) => {
          if (err) throw new Meteor.Error('import-sec', err.message);
          if (!err) {
            Containers.remove({});

            _.each(data.slice(2), (e) => {
              if (!_.isEmpty(e)) {
                let c = {
                  location: String(e[1]),
                  inDate: moment(e[7], 'DD/MM/YYYY HH:mm:ss').toDate(),
                };

                if (!_.isEmpty(e[2])) c = _.extend(c, { po: Number(e[2]) });
                if (!_.isEmpty(e[3])) c = _.extend(c, { article: Number(e[3]) });
                if (!_.isEmpty(e[4])) c = _.extend(c, { machine: Number(e[4]) });
                if (!_.isEmpty(e[5])) c = _.extend(c, { lot: Number(e[5]) });
                if (!_.isEmpty(e[6])) c = _.extend(c, { parts: Number(e[6]) });
                if (!_.isEmpty(e[8])) c = _.extend(c, { requestDate: moment(e[8], 'DD/MM/YYYY HH:mm:ss').toDate() });
                if (!_.isEmpty(e[9])) c = _.extend(c, { outDate: moment(e[9], 'DD/MM/YYYY HH:mm:ss').toDate() });

                Containers.insert(c);
                Emplacments.upsert({ _id: String(e[1]) }, { _id: String(e[1]) });
              }
            });
          }
        }));
      },
      'containers.mvtIn'() { // eslint-disable-line
        const mvtIn = Containers.aggregate([{
          $group: {
            _id: {
              yearA: { $year: '$inDate' },
              monthA: { $month: '$inDate' },
              dayA: { $dayOfMonth: '$inDate' },
            },
            count: { $sum: 1 },
          },
        }]);

        return mvtIn;
      },
      'containers.mvtOut'() { // eslint-disable-line
        const mvtOut = Containers.aggregate([{
          $match: {
            outDate: { $ne: null },
          },
        }, {
          $group: {
            _id: {
              yearA: { $year: '$outDate' },
              monthA: { $month: '$outDate' },
              dayA: { $dayOfMonth: '$outDate' },
            },
            count: { $sum: 1 },
          },
        }]);

        return mvtOut;
      },
      'containers.mvt'(start, end) { // eslint-disable-line
        Mvt.remove({});

        const mvtIn = Containers.aggregate([{
          $match: {
            $and: [
              { inDate: { $ne: null } },
              { inDate: { $gte: start } },
              { inDate: { $lte: end } },
            ],
          },
        }, {
          $group: {
            _id: {
              yearA: { $year: '$inDate' },
              monthA: { $month: '$inDate' },
              dayA: { $dayOfMonth: '$inDate' },
            },
            count: { $sum: 1 },
          },
        }]);

        mvtIn.forEach((e) => {
          Mvt.insert({
            date: moment().date(e._id.dayA).month(e._id.monthA - 1).year(e._id.yearA)
            .hour(0)
            .minute(0)
            .second(0)
            .millisecond(0)
            .toDate(),
            year: e._id.yearA,
            month: e._id.monthA,
            day: e._id.dayA,
            in: e.count,
          });
        });

        const mvtOut = Containers.aggregate([{
          $match: {
            $and: [
              { outDate: { $ne: null } },
              { outDate: { $gte: start } },
              { outDate: { $lte: end } },
            ],
          },
        }, {
          $group: {
            _id: {
              yearA: { $year: '$outDate' },
              monthA: { $month: '$outDate' },
              dayA: { $dayOfMonth: '$outDate' },
            },
            count: { $sum: 1 },
          },
        }]);

        mvtOut.forEach((e) => {
          Mvt.upsert({
            year: e._id.yearA,
            month: e._id.monthA,
            day: e._id.dayA,
          }, { $set: {
            date: moment().date(e._id.dayA).month(e._id.monthA - 1).year(e._id.yearA)
            .hour(0)
            .minute(0)
            .second(0)
            .millisecond(0)
            .toDate(),
            year: e._id.yearA,
            month: e._id.monthA,
            day: e._id.dayA,
            out: e.count,
          },
        });
        });

        return Mvt.find({}, { sort: { date: 1 } }).fetch();
      },
    });
  }
