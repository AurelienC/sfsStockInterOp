import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Save = new Mongo.Collection('save');

// Add default values
if (Meteor.isServer) {
  Meteor.startup(() => {
    if (_.isUndefined(Save.findOne())) {
      Save.remove({});
      Save.insert({
        ftp: {
          active: false,
          host: 'localhost',
          user: 'user',
          password: 'password',
        },
        local: {
          active: false,
          directory: '/var/log/supervision',
        },
        smb: {
          active: false,
          share: '\\\\chsfscif200.sfs-intra.net\\transfer$',
          directory: 'caau',
          domain: 'SFS-INTRA',
          user: 'caau',
          password: 'caau',
        },
      });
    }
  });
}


if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('save', () => { // eslint-disable-line
    return Save.find();
  });

  Meteor.methods({
    'save.update'(id, save) { // eslint-disable-line
      check(id, String);
      check(save.ftp.active, Boolean);
      check(save.ftp.host, String);
      check(save.ftp.user, String);
      check(save.ftp.password, String);
      check(save.local.active, Boolean);
      check(save.local.directory, String);
      check(save.smb.active, Boolean);
      check(save.smb.share, String);
      check(save.smb.directory, String);
      check(save.smb.domain, String);
      check(save.smb.user, String);
      check(save.smb.password, String);

      Save.update(id, { $set: save });
    },
  });
}
