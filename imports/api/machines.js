import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Machines = new Mongo.Collection('machines');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('machines', () => { // eslint-disable-line
    return Machines.find();
  });

  Meteor.methods({
    'machines.insert'(id, emplacment) { // eslint-disable-line
      check(id, Number);
      check(emplacment, String);

      Machines.insert({
        _id: id.toString(),
        emplacment,
      });
    },
    'machines.remove'(id) { // eslint-disable-line
      check(id, Number);
      Machines.remove({ _id: id.toString() });
    },
  });
}
