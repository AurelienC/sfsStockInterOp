import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check, Match } from 'meteor/check';

export const Emplacments = new Mongo.Collection('emplacments');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('emplacments', () => { // eslint-disable-line
    return Emplacments.find();
  });

// Emplacment Pattern
const patt = /^[0-9]{2}[A-Z][0-9]{2}$/;
const emplacmentMatch = Match.Where(x => patt.test(x));

  Meteor.methods({
    'emplacments.insert'(id, emplacment) { // eslint-disable-line
      check(id, emplacmentMatch);

      Emplacments.insert({
        _id: id.toString(),
      });
    },
    'emplacments.remove'(id) { // eslint-disable-line
      check(id, emplacmentMatch);
      Emplacments.remove({ _id: id });
    },
  });
}
